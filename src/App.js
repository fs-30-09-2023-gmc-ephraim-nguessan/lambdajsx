import logo from './logo.svg';
import './App.css';
import Bloc from './Components/Bloc/Bloc';


function App() {
  return (
    <div className="App">
      <Bloc/>
    </div>
  );
}

export default App;
