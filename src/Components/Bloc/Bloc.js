import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {Nom, premiernom} from "../Nom/Nom";
import CheminVersImage from "../CheminVersImage/CheminversImage";
import Description from "../Description/Description";
import Prix from "../Prix/Prix";
const img="tomate.jpg"


function Bloc() {
    return (
        <Card style={{ width: "18rem" }}>

            <Card.Img variant="top" src={img!=null ? img: ""} />
            <Card.Body>
                <Card.Title>Mon nom:<Nom/></Card.Title>
                <Card.Text>
                    Ma description: <Description/>
                </Card.Text>
                <Card.Text>
                    Prix: <Prix/>
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>
            <div>
                {/* premiere façon de faire */}
                {/* {premiernom!=null ? "Bonjour " + premiernom :"Bonjour!"} */}
                
                {/* deuxieme façon de faire */}
                {premiernom!=null ? `Bonjour ${premiernom}`:"Bonjour!"}
            </div>
        </Card>
    );
}

export default Bloc;
